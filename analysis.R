library(tidyverse)

hotelbooking <- read.csv('hotel_bookings.csv')


data <- hotelbooking

data$reservation_status[data$reservation_status != 'Canceled'] <- 'Not Canceled'

res <- chisq.test(table(data$hotel, data$reservation_status))
res

res$p.value
res$statistic

res$parameter

res1 <- fisher.test(table(data$hotel, data$reservation_status))

res1$p.value
res1$conf.int
res1$estimate
